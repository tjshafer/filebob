<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('addons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('api_key');
            $table->string('logo');
            $table->string('name');
            $table->string('symbol');
            $table->string('version', 50);
            $table->string('action_text')->nullable();
            $table->string('action_link')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
        });
    }
};
