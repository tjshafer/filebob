<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('payment_gateways', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('symbol', 100);
            $table->string('handler', 255);
            $table->string('logo', 255);
            $table->tinyInteger('fees');
            $table->boolean('test_mode')->nullable()->comment('null 0:Disbaled 1:Enabled');
            $table->text('credentials');
            $table->text('instructions')->nullable();
            $table->boolean('status')->default(false)->comment('0:Disabled 1:Active');
            $table->timestamps();
        });
    }
};
