<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('download_links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('file_entry_id')->unsigned();
            $table->timestamp('expiry_at')->nullable();
            $table->foreign('file_entry_id')->references('id')->on('file_entries')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }
};
