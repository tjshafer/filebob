<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->id();
            $table->string('lang');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('content');
            $table->string('short_description', 200);
            $table->timestamps();
        });
    }
};
