<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('footer_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lang', 3);
            $table->string('name', 100)->unique();
            $table->text('link');
            $table->integer('sort_id');

            $table->timestamps();
        });
    }
};
