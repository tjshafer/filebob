<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('slideshows', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('type')->comment('1:Image 2:Video');
            $table->tinyInteger('source')->comment('1:Upload 2:URL');
            $table->text('file');
            $table->integer('duration');
            $table->timestamps();
        });
    }
};
