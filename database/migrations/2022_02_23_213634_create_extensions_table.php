<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('extensions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('symbol');
            $table->string('logo');
            $table->longText('credentials');
            $table->text('instructions')->nullable();
            $table->boolean('status')->default(false)->comment('0:Disabled 1:Enabled');
            $table->timestamps();
        });
    }
};
