<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('translates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lang', 3);
            $table->string('group_name')->default('general');
            $table->text('key');
            $table->text('value')->nullable();

            $table->timestamps();
        });
    }
};
