<?php

namespace App\Models;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileEntry extends Model
{
    use HasFactory;

    /**
     * Get the current user entries
     *
     * @return $id
     */
    public function scopeCurrentUser($query): void
    {
        $query->where('user_id', userAuthInfo()->id);
    }

    /**
     * Get only none expired
     *
     * @return $id
     */
    public function scopeNotExpired($query): void
    {
        $query->where(function ($query) {
            $query->where('expiry_at', '>', Carbon::now())->orWhereNull('expiry_at');
        });
    }

    /**
     * Get users entries
     *
     * @return $id
     */
    public function scopeUserEntry($query): void
    {
        $query->where('user_id', '!=', null);
    }

    /**
     * Get guests entries
     *
     * @return $id
     */
    public function scopeGuestEntry($query): void
    {
        $query->where('user_id', null);
    }

    /**
     * File can be previewed
     *
     * @return $id
     */
    public function scopeHasPreview($query): void
    {
        $query->whereIn('type', ['image', 'pdf']);
    }

    /**
     * File without parent
     *
     * @return $id
     */
    public function scopeHasNoParent($query): void
    {
        $query->where('parent_id', null);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'ip',
        'shared_id',
        'user_id',
        'storage_provider_id',
        'name',
        'filename',
        'mime',
        'size',
        'extension',
        'type',
        'path',
        'link',
        'access_status',
        'password',
        'downloads',
        'views',
        'admin_has_viewed',
        'expiry_at',
    ];

    protected $casts = [
        'expiry_at' => 'datetime',
    ];

    /**
     * Relationships
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function storageProvider(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(StorageProvider::class, 'storage_provider_id', 'id');
    }

    public function reports(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(FileReport::class, 'file_entry_id', 'id');
    }
}
