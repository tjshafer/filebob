<?php

namespace App\Models;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DownloadLink extends Model
{
    use HasFactory;

    /**
     * Get only expired
     *
     * @return $id
     */
    public function scopeHasExpired($query): void
    {
        $query->where(function ($query) {
            $query->where('expiry_at', '<', Carbon::now());
        });
    }

    /**
     * Get only none expired
     *
     * @return $id
     */
    public function scopeNotExpired($query): void
    {
        $query->where(function ($query) {
            $query->where('expiry_at', '>', Carbon::now());
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'file_entry_id',
        'expiry_at',
    ];

    public function fileEntry(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(FileEntry::class, 'file_entry_id', 'id');
    }
}
