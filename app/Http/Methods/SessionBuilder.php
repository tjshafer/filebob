<?php

namespace App\Http\Methods;

use Illuminate\Support\Str;

class SessionBuilder
{
    /**
     * Create a specific prefix for every session
     *
     * This function used in sessions config to set the prefix
     *
     * @return $prefix
     */
    public static function sessionName(): string
    {
        if (isset($_SERVER['REQUEST_URI'])) {
            $uriSegments = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            if (Str::of($uriSegments)->contains('admin/')) {
                $name = Str::of(config('app.name'))->slug('_').'_admin_session';
            } else {
                $name = Str::of(config('app.name'))->slug('_').'_user_session';
            }
        } else {
            $name = Str::of(config('app.name'))->slug('_').'_session';
        }

        return $name;
    }
}
