<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use App\Models\Language;
use Illuminate\Support\Facades\Session;

class LocalizationController extends Controller
{
    public function localize(Request $request, $code)
    {
        if (! settings('website_language_type')) {
            $language = Language::where('code', $code)->firstOrFail();
            App::setLocale($language->code);
            $request->session()->forget('locale');
            $request->session()->put('locale', $language->code);

            return redirect()->back();
        }
    }
}
