<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;

class ExtraController extends Controller
{
    public function cookie(): \Illuminate\Http\JsonResponse
    {
        Cookie::queue('cookie_accepted', true, time() + 31556926);

        return response()->json(['success' => lang('Cookie accepted successfully', 'alerts')]);
    }

    public function popup(): void
    {
        Cookie::queue('popup_closed', true, 1440);
    }
}
