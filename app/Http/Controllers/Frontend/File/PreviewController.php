<?php

namespace App\Http\Controllers\Frontend\File;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FileEntry;
use Illuminate\Support\Facades\Session;

class PreviewController extends Controller
{
    public function index(Request $request, $shared_id): \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $fileEntry = FileEntry::where('shared_id', $shared_id)->notExpired()->hasPreview()->with('user')->firstOrFail();
        abort_if(! DownloadController::accessCheck($fileEntry), 404);
        if (! is_null($fileEntry->password)) {
            if (! $request->session()->has(filePasswordSession($fileEntry->shared_id))) {
                return redirect(route('file.password', $fileEntry->shared_id).'?source=preview');
            }
            $password = decrypt($request->session()->get(filePasswordSession($fileEntry->shared_id)));
            if ($password != $fileEntry->password) {
                return redirect(route('file.password', $fileEntry->shared_id).'?source=preview');
            }
        }
        $fileEntry->increment('views');
        $view = ($fileEntry->type == 'pdf') ? 'frontend.file.preview.pdf' : 'frontend.file.preview.image';

        return view($view, ['fileEntry' => $fileEntry]);
    }
}
