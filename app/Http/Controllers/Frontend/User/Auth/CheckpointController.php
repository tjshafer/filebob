<?php

namespace App\Http\Controllers\Frontend\User\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CheckpointController extends Controller
{
    protected function user()
    {
        $user = User::find(userAuthInfo()->id);

        return $user;
    }

    public function show2FaVerifyForm(Request $request): \Illuminate\Http\RedirectResponse|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if ($this->user()->google2fa_status) {
            if ($request->session()->has('2fa')) {
                return to_route('user.dashboard');
            }
        } else {
            return to_route('user.dashboard');
        }

        return view('frontend.user.auth.checkpoint.2fa');
    }

    public function verify2fa(Request $request): \Illuminate\Http\RedirectResponse
    {
        $validator = Validator::make($request->all(), [
            'otp_code' => ['required', 'numeric'],
        ]);

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                toastr()->error($error);
            }

            return redirect()->back();
        }
        $google2fa = app('pragmarx.google2fa');
        $valid = $google2fa->verifyKey($this->user()->google2fa_secret, $request->otp_code);
        if ($valid == false) {
            toastr()->error(lang('Invalid OTP code', 'alerts'));

            return redirect()->back();
        }
        $request->session()->put('2fa', $this->user()->id);

        return to_route('user.dashboard');
    }
}
