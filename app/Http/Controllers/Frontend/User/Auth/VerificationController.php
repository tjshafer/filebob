<?php

namespace App\Http\Controllers\Frontend\User\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
     */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    /**
     * Show the email verification notice.
     */
    public function show(Request $request): \Illuminate\Http\RedirectResponse|\Illuminate\View\View
    {
        if (! settings('website_email_verify_status')) {
            return to_route('user.dashboard');
        }
        return $request->user()->hasVerifiedEmail()
        ? redirect()->to($this->redirectPath())
        : view('frontend.user.auth.verify');
    }

    /**
     * Change email address
     */
    public function changeEmail(Request $request)
    {
        $user = User::find(userAuthInfo()->id);
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:100', 'unique:users,email,'.$user->id],
        ]);
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                toastr()->error($error);
            }

            return redirect()->back();
        }
        if ($user->email == $request->email) {
            toastr()->error(lang('You must to change the email to make a change', 'alerts'));

            return redirect()->back();
        }
        $update = $user->update(['email' => $request->email]);
        if ($update) {
            $user->sendEmailVerificationNotification();
            toastr()->success(lang('Email has been changed successfully', 'alerts'));

            return redirect()->back();
        }
    }
}
