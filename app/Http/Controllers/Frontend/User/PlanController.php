<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PlanController extends Controller
{
    public function index(Request $request): \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        if (! userAuthInfo()->subscription && $request->st != 'subscribe') {
            return redirect(route('user.plans').'?st=subscribe');
        }
        if (userAuthInfo()->subscription && $request->st) {
            return to_route('user.plans');
        }

        return view('frontend.user.plans.index');
    }
}
