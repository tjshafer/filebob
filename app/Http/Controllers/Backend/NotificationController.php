<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\AdminNotification;

class NotificationController extends Controller
{
    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $notifications = AdminNotification::orderbyDesc('id')->paginate(10);
        $unreadNotificationsCount = AdminNotification::where('status', 0)->get()->count();

        return view('backend.notifications.index', ['notifications' => $notifications, 'unreadNotificationsCount' => $unreadNotificationsCount]);
    }

    public function view($id)
    {
        $notification = AdminNotification::where('id', unhashid($id))->firstOrFail();
        $updateStatus = $notification->update(['status' => 1]);
        if ($updateStatus) {
            return redirect()->to($notification->link);
        }
    }

    public function readAll(): \Illuminate\Http\RedirectResponse
    {
        $notifications = AdminNotification::where('status', 0)->get();
        if ($notifications->count() == 0) {
            toastr()->error(__('No unread notifications available'));

            return redirect()->back();
        }
        foreach ($notifications as $notification) {
            $notification->update(['status' => 1]);
        }
        toastr()->success(__('All notifications has been read successfully'));

        return redirect()->back();
    }

    public function deleteAllRead(): \Illuminate\Http\RedirectResponse
    {
        $notifications = AdminNotification::where('status', 1)->get();
        if ($notifications->count() == 0) {
            toastr()->error(__('No read notifications available'));

            return redirect()->back();
        }
        foreach ($notifications as $notification) {
            $notification->delete();
        }
        toastr()->success(__('Deleted Successfully'));

        return redirect()->back();
    }
}
