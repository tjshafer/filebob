<?php

namespace App\Http\Controllers\Backend\Others;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('lang')) {
            $language = Language::where('code', $request->lang)->firstOrFail();
            $faqs = Faq::where('lang', $language->code)->get();

            return view('backend.others.faq.index', ['faqs' => $faqs, 'active' => $language->name]);
        }
        return redirect(url()->current().'?lang='.config('settings.default_language'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('backend.others.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lang' => ['required', 'string', 'max:3'],
            'title' => ['required', 'string', 'max:255'],
            'content' => ['required', 'string'],
        ]);
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                toastr()->error($error);
            }

            return redirect()->back()->withInput();
        }
        $lang = Language::where('code', $request->lang)->first();
        if ($lang == null) {
            toastr()->error(__('Language not exists'));

            return redirect()->back();
        }
        $create = Faq::create([
            'lang' => $lang->code,
            'title' => $request->title,
            'content' => $request->content,
        ]);
        if ($create) {
            toastr()->success(__('Created Successfully'));

            return redirect(route('admin.faq.index').'?lang='.$create->lang);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Faq $faq): \Illuminate\Http\Response
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Faq $faq): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('backend.others.faq.edit', ['faq' => $faq]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq)
    {
        $validator = Validator::make($request->all(), [
            'lang' => ['required', 'string', 'max:3'],
            'title' => ['required', 'string', 'max:255'],
            'content' => ['required', 'string'],
        ]);
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                toastr()->error($error);
            }

            return redirect()->back()->withInput();
        }
        $lang = Language::where('code', $request->lang)->first();
        if ($lang == null) {
            toastr()->error(__('Language not exists'));

            return redirect()->back();
        }
        $update = $faq->update([
            'lang' => $lang->code,
            'title' => $request->title,
            'content' => $request->content,
        ]);
        if ($update) {
            toastr()->success(__('Updated Successfully'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Faq $faq): \Illuminate\Http\RedirectResponse
    {
        $faq->delete();
        toastr()->success(__('Deleted Successfully'));

        return redirect()->back();
    }
}
