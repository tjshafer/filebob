<?php

namespace App\Http\Controllers\Backend\Blog;

use App\Http\Controllers\Controller;
use App\Models\BlogArticle;
use App\Models\BlogCategory;
use App\Models\Language;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('lang')) {
            $language = Language::where('code', $request->lang)->firstOrFail();
            $articles = BlogArticle::where('lang', $language->code)->with(['blogCategory', 'admin'])->withCount('comments')->get();

            return view('backend.blog.articles.index', ['articles' => $articles, 'active' => $language->name]);
        }
        return redirect(url()->current().'?lang='.config('settings.default_language'));
    }

    /**
     * Create a blog article slug using ajax request
     */
    public function slug(Request $request): \Illuminate\Http\JsonResponse
    {
        $slug = null;
        if ($request->content != null) {
            $slug = SlugService::createSlug(BlogArticle::class, 'slug', $request->content);
        }

        return response()->json(['slug' => $slug]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $categories = BlogCategory::orderbyDesc('id')->get();

        return view('backend.blog.articles.create', ['categories' => $categories]);
    }

    /**
     * Get categories by lang
     *
     * @return \Illuminate\Http\Response as JSON
     */
    public function getCategories($lang)
    {
        $categories = BlogCategory::where('lang', $lang)->pluck('name', 'id');
        if ($categories->count() > 0) {
            return response()->json($categories);
        }
        return response()->json(['info' => __('No categories on this language')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lang' => ['required', 'string', 'max:3'],
            'title' => ['required', 'string', 'max:255', 'min:2'],
            'slug' => ['required', 'unique:blog_articles', 'alpha_dash'],
            'image' => ['required', 'image', 'mimes:png,jpg,jpeg', 'max:2048'],
            'category' => ['required', 'numeric'],
            'content' => ['required'],
            'short_description' => ['required', 'string', 'max:200', 'min:2'],
        ]);
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                toastr()->error($error);
            }

            return redirect()->back()->withInput();
        }
        $category = BlogCategory::where('id', $request->category)->first();
        if ($category == null) {
            toastr()->error(__('Something went wrong please try again'));

            return redirect()->back();
        }
        $lang = Language::where('code', $request->lang)->first();
        if ($lang == null) {
            toastr()->error(__('Language not exists'));

            return redirect()->back();
        }
        if ($category->lang != $lang->code) {
            toastr()->error(__('Category and article must be in the same language'));

            return redirect()->back();
        }
        $uploadImage = vImageUpload($request->file('image'), 'images/blog/articles/', '900x450');
        if ($uploadImage) {
            $create = BlogArticle::create([
                'lang' => $lang->code,
                'admin_id' => adminAuthInfo()->id,
                'title' => $request->title,
                'slug' => SlugService::createSlug(BlogArticle::class, 'slug', $request->title),
                'image' => $uploadImage,
                'category_id' => $category->id,
                'content' => $request->content,
                'short_description' => $request->short_description,
            ]);
            if ($create) {
                toastr()->success(__('Created Successfully'));

                return redirect(route('articles.index').'?lang='.$create->lang);
            }
        } else {
            toastr()->error(__('Upload error'));

            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(BlogArticle $article): \Illuminate\Http\Response
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(BlogArticle $article): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $categories = BlogCategory::where('lang', $article->lang)->orderbyDesc('id')->get();

        return view('backend.blog.articles.edit', ['article' => $article, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BlogArticle $article)
    {
        $validator = Validator::make($request->all(), [
            'lang' => ['required', 'string', 'max:3'],
            'title' => ['required', 'string', 'max:255', 'min:2'],
            'slug' => ['required', 'alpha_dash', 'unique:blog_articles,slug,'.$article->id],
            'image' => ['mimes:png,jpg,jpeg', 'max:2048'],
            'category' => ['required', 'numeric'],
            'content' => ['required'],
            'short_description' => ['required', 'string', 'max:200', 'min:2'],
        ]);
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                toastr()->error($error);
            }

            return redirect()->back();
        }
        $category = BlogCategory::where('id', $request->category)->first();
        if ($category == null) {
            toastr()->error(__('Something went wrong please try again'));

            return redirect()->back();
        }
        $lang = Language::where('code', $request->lang)->first();
        if ($lang == null) {
            toastr()->error(__('Language not exists'));

            return redirect()->back();
        }
        if ($category->lang != $lang->code) {
            toastr()->error(__('Category and article must be in the same language'));

            return redirect()->back();
        }
        if ($request->has('image')) {
            $uploadImage = vImageUpload($request->file('image'), 'images/blog/articles/', '900x450', null, $article->image);
        } else {
            $uploadImage = $article->image;
        }
        if ($uploadImage) {
            $update = $article->update([
                'lang' => $lang->code,
                'title' => $request->title,
                'slug' => $request->slug,
                'image' => $uploadImage,
                'category_id' => $category->id,
                'content' => $request->content,
                'short_description' => $request->short_description,
            ]);
            if ($update) {
                toastr()->success(__('Updated Successfully'));

                return redirect()->back();
            }
        } else {
            toastr()->error(__('Upload error'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(BlogArticle $article): \Illuminate\Http\RedirectResponse
    {
        removeFile($article->image);
        $article->delete();
        toastr()->success(__('Deleted Successfully'));

        return redirect()->back();
    }
}
