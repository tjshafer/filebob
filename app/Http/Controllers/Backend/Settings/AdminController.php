<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\BlogArticle;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $admins = Admin::where('id', '!=', adminAuthInfo()->id)->get();

        return view('backend.settings.admins.index', ['admins' => $admins]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $password = Str::random(15);

        return view('backend.settings.admins.create', ['password' => $password]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'avatar' => ['image', 'mimes:png,jpg,jpeg'],
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'unique:admins'],
            'password' => ['required', 'min:8'],
        ]);
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                toastr()->error($error);
            }

            return redirect()->back()->withInput();
        }
        if ($request->has('avatar')) {
            $uploadAvatar = vImageUpload($request->file('avatar'), 'images/avatars/admins/', '110x110');
        } else {
            $uploadAvatar = 'images/avatars/default.png';
        }
        $create = Admin::create([
            'avatar' => $uploadAvatar,
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        if ($create) {
            toastr()->success(__('Created Successfully'));

            return to_route('admins.index');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Admin $admin): \Illuminate\Http\Response
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Admin $admin): \Illuminate\Http\Response
    {
        if ($admin->id == adminAuthInfo()->id) {
            return abort(404);
        }

        return view('backend.settings.admins.edit', ['admin' => $admin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        if ($admin->id == adminAuthInfo()->id) {
            toastr()->error(__('Something went wrong please try again'));

            return redirect()->back()->withInput();
        }
        $validator = Validator::make($request->all(), [
            'avatar' => ['image', 'mimes:png,jpg,jpeg'],
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'unique:admins,email,'.$admin->id],
        ]);
        if ($request->has('password') && $request->password != null) {
            $validator = Validator::make($request->all(), [
                'password' => ['min:8'],
            ]);
            $password = bcrypt($request->password);
        } else {
            $password = $admin->password;
        }
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                toastr()->error($error);
            }

            return redirect()->back();
        }
        if ($request->has('avatar')) {
            if ($admin->avatar == 'images/avatars/default.png') {
                $uploadAvatar = vImageUpload($request->file('avatar'), 'images/avatars/admins/', '110x110');
            } else {
                $uploadAvatar = vImageUpload($request->file('avatar'), 'images/avatars/admins/', '110x110', null, $admin->avatar);
            }
        } else {
            $uploadAvatar = $admin->avatar;
        }
        $update = $admin->update([
            'avatar' => $uploadAvatar,
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'email' => $request->email,
            'password' => $password,
        ]);
        if ($update) {
            toastr()->success(__('Updated Successfully'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Admin $admin): \Illuminate\Http\RedirectResponse
    {
        $articles = BlogArticle::where('admin_id', $admin->id)->get();
        if ($articles->count() >= 1) {
            foreach ($articles as $article) {
                removeFile($article->image);
            }
        }
        if ($admin->avatar != 'images/avatars/default.png') {
            removeFile($admin->avatar);
        }
        $admin->delete();
        toastr()->success(__('Deleted Successfully'));

        return redirect()->back();
    }
}
