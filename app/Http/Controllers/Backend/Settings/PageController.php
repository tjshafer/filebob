<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\Page;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('lang')) {
            $language = Language::where('code', $request->lang)->firstOrFail();
            $pages = Page::where('lang', $language->code)->get();

            return view('backend.settings.pages.index', ['pages' => $pages, 'active' => $language->name]);
        }
        return redirect(url()->current().'?lang='.config('settings.default_language'));
    }

    /**
     * Create a page slug using ajax request
     */
    public function slug(Request $request): \Illuminate\Http\JsonResponse
    {
        $slug = null;
        if ($request->content != null) {
            $slug = SlugService::createSlug(Page::class, 'slug', $request->content);
        }

        return response()->json(['slug' => $slug]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('backend.settings.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            Page::VALIDATION_RULES + ['slug' => ['required', 'unique:pages', 'alpha_dash']]
        );
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                toastr()->error($error);
            }

            return redirect()->back()->withInput();
        }
        $lang = Language::where('code', $request->lang)->first();
        if ($lang == null) {
            toastr()->error(__('Language not exists'));

            return redirect()->back();
        }
        $create = Page::create([
            'lang' => $lang->code,
            'title' => $request->title,
            'slug' => SlugService::createSlug(Page::class, 'slug', $request->title),
            'content' => $request->content,
            'short_description' => $request->short_description,
        ]);
        if ($create) {
            toastr()->success(__('Created Successfully'));

            return redirect(route('pages.index').'?lang='.$create->lang);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Page $page): \Illuminate\Http\Response
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Page $page): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('backend.settings.pages.edit', ['page' => $page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $validator = Validator::make($request->all(),
            Page::VALIDATION_RULES + ['slug' => ['required', 'alpha_dash', 'unique:pages,slug,'.$page->id]]
        );
        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                toastr()->error($error);
            }

            return redirect()->back();
        }
        $lang = Language::where('code', $request->lang)->first();
        if ($lang == null) {
            toastr()->error(__('Language not exists'));

            return redirect()->back();
        }
        $update = $page->update([
            'lang' => $lang->code,
            'title' => $request->title,
            'slug' => $request->slug,
            'content' => $request->content,
            'short_description' => $request->short_description,
        ]);
        if ($update) {
            toastr()->success(__('Updated Successfully'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Page $page): \Illuminate\Http\RedirectResponse
    {
        $page->delete();
        toastr()->success(__('Deleted Successfully'));

        return redirect()->back();
    }
}
