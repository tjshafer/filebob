<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;
use App\Models\StorageProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StorageController extends Controller
{
    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $storageProviders = StorageProvider::all();

        return view('backend.settings.storage.index', ['storageProviders' => $storageProviders]);
    }

    public function edit($id): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $storageProvider = StorageProvider::findOrFail($id);
        abort_if($storageProvider->symbol == 'local', 404);

        return view('backend.settings.storage.edit', ['storageProvider' => $storageProvider]);
    }

    public function update(Request $request, $id)
    {
        $storageProvider = StorageProvider::find($id);
        if (! $storageProvider && $storageProvider->symbol == 'local') {
            toastr()->error(__('Provider not exists'));

            return redirect()->back();
        }
        foreach ($request->credentials as $key => $value) {
            if (! array_key_exists($key, (array) $storageProvider->credentials)) {
                toastr()->error(__('Credentials parameter error'));

                return redirect()->back();
            }
        }
        if ($request->has('status')) {
            foreach ($request->credentials as $key => $value) {
                if (empty($value)) {
                    toastr()->error(str_replace('_', ' ', $key).__(' cannot be empty'));

                    return redirect()->back();
                }
            }
            $request->status = 1;
        } else {
            if (config('filesystems.default') == $storageProvider->symbol) {
                toastr()->error(__('Default provider cannot disabled'));

                return redirect()->back();
            }
            $request->status = 0;
        }
        $update = $storageProvider->update([
            'status' => $request->status,
            'credentials' => $request->credentials,
        ]);
        if ($update) {
            $storageProvider->handler::setCredentials($storageProvider);
            toastr()->success(__('Updated Successfully'));

            return redirect()->back();
        }
    }

    public function setDefault(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $storageProvider = StorageProvider::findOrFail($id);
        if (! $storageProvider->status) {
            toastr()->error($storageProvider->name.__(' status is disabled'));

            return redirect()->back();
        }
        setEnv('FILESYSTEM_DRIVER', $storageProvider->symbol);
        toastr()->success(__($storageProvider->name.' is now default storage'));

        return redirect()->back();
    }

    public function storageTest($id)
    {
        $storageProvider = StorageProvider::find($id);
        if (! $storageProvider && $storageProvider->symbol == 'local') {
            toastr()->error(__('Provider not exists'));

            return redirect()->back();
        }
        if (! $storageProvider->status) {
            toastr()->error($storageProvider->name.__(' status is disabled'));

            return redirect()->back();
        }
        try {
            $upload = Storage::disk($storageProvider->symbol)->put('test.txt', 'public');
            if ($upload) {
                Storage::disk($storageProvider->symbol)->delete('test.txt');
                toastr()->success($storageProvider->name.__(' Connected Successfully'));

                return redirect()->back();
            }
            toastr()->error($storageProvider->name.__(' Connection error'));
            return redirect()->back();
        } catch (\Exception$e) {
            toastr()->error($storageProvider->name.__(' Connection error'));

            return redirect()->back();
        }
    }
}
