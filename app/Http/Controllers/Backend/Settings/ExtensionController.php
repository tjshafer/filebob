<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;
use App\Http\Methods\ExtentionCredentials;
use App\Models\Extension;
use Illuminate\Http\Request;

class ExtensionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $extensions = Extension::all();

        return view('backend.settings.extensions.index', ['extensions' => $extensions]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Extension $extension): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('backend.settings.extensions.edit', ['extension' => $extension]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Extension $extension)
    {
        foreach ($request->credentials as $key => $value) {
            if (! array_key_exists($key, (array) $extension->credentials)) {
                toastr()->error(__('Credentials parameter error'));

                return redirect()->back();
            }
        }

        if ($request->has('status')) {
            foreach ($request->credentials as $key => $value) {
                if (empty($value)) {
                    toastr()->error(str_replace('_', ' ', $key).__(' cannot be empty'));

                    return redirect()->back();
                }
            }
            $request->status = 1;
        } else {
            $request->status = 0;
        }
        $update = $extension->update([
            'status' => $request->status,
            'credentials' => $request->credentials,
        ]);
        if ($update) {
            ExtentionCredentials::credentials($extension);
            toastr()->success(__('Updated Successfully'));

            return redirect()->back();
        }
    }
}
