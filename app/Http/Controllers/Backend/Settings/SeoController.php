<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\SeoConfiguration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SeoController extends Controller
{
    private array $robots_index_array = ['index', 'noindex'];

    private array $robots_follow_links_array = ['follow', 'nofollow'];

    /**
     * Display a listing of the resource.
     */
    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('backend.settings.seo.index', ['configurations' => SeoConfiguration::all()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $languages = Language::orderbyDesc('id')->get();

        return view('backend.settings.seo.create', ['languages' => $languages]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lang' => ['required', 'string', 'unique:seo_configurations'],
            'title' => ['required', 'string', 'max:70'],
            'description' => ['required', 'string', 'max:150'],
            'keywords' => ['required', 'string', 'max:255'],
            'robots_index' => ['required', 'string', 'max:50'],
            'robots_follow_links' => ['required', 'string', 'max:50'],
        ]);

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                toastr()->error($error);
            }

            return redirect()->back()->withInput();
        }

        $lang = Language::where('code', $request->lang)->first();
        if ($lang == null) {
            toastr()->error(__('Language not exists'));

            return redirect()->back();
        }

        if (! in_array($request->robots_index, $this->robots_index_array)) {
            toastr()->error(__('Something went wrong please try again'));

            return redirect()->back();
        }

        if (! in_array($request->robots_follow_links, $this->robots_follow_links_array)) {
            toastr()->error(__('Something went wrong please try again'));

            return redirect()->back();
        }

        $create = SeoConfiguration::create([
            'lang' => $lang->code,
            'title' => $request->title,
            'description' => $request->description,
            'keywords' => $request->keywords,
            'robots_index' => $request->robots_index,
            'robots_follow_links' => $request->robots_follow_links,
        ]);

        if ($create) {
            toastr()->success(__('Created Successfully'));

            return to_route('seo.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SeoConfiguration  $seoConfiguration
     */
    public function show(SeoConfiguration $seo): \Illuminate\Http\Response
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SeoConfiguration  $seoConfiguration
     */
    public function edit(SeoConfiguration $seo): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $languages = Language::orderbyDesc('id')->get();

        return view('backend.settings.seo.edit', ['languages' => $languages, 'configuration' => $seo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Models\SeoConfiguration  $seoConfiguration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SeoConfiguration $seo)
    {
        $validator = Validator::make($request->all(), [
            'lang' => ['required', 'string', 'unique:seo_configurations,lang,'.$seo->id],
            'title' => ['required', 'string', 'max:70'],
            'description' => ['required', 'string', 'max:150'],
            'keywords' => ['required', 'string', 'max:255'],
            'robots_index' => ['required', 'string', 'max:50'],
            'robots_follow_links' => ['required', 'string', 'max:50'],
        ]);

        if ($validator->fails()) {
            foreach ($validator->errors()->all() as $error) {
                toastr()->error($error);
            }

            return redirect()->back();
        }

        $lang = Language::where('code', $request->lang)->first();
        if ($lang == null) {
            toastr()->error(__('Language not exists'));

            return redirect()->back();
        }

        if (! in_array($request->robots_index, $this->robots_index_array)) {
            toastr()->error(__('Something went wrong please try again'));

            return redirect()->back();
        }

        if (! in_array($request->robots_follow_links, $this->robots_follow_links_array)) {
            toastr()->error(__('Something went wrong please try again'));

            return redirect()->back();
        }

        $update = $seo->update([
            'lang' => $lang->code,
            'title' => $request->title,
            'description' => $request->description,
            'keywords' => $request->keywords,
            'robots_index' => $request->robots_index,
            'robots_follow_links' => $request->robots_follow_links,
        ]);

        if ($update) {
            toastr()->success(__('Updated Successfully'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SeoConfiguration  $seoConfiguration
     */
    public function destroy(SeoConfiguration $seo): \Illuminate\Http\RedirectResponse
    {
        $seo->delete();
        toastr()->success(__('Deleted Successfully'));

        return redirect()->back();
    }
}
