<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;
use Illuminate\Http\Request;

class RedirectNotAdmin
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next, $guard = 'admin')
    {
        if (! Auth::guard($guard)->check()) {
            return to_route('admin.login');
        }

        return $next($request);
    }
}
