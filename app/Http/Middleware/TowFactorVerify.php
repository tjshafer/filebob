<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;
use Illuminate\Http\Request;

class TowFactorVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     */
    public function handle(Request $request, Closure $next): \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
    {
        if ($request->user() && userAuthInfo()->google2fa_status && ! $request->session()->has('2fa') &&
            session('2fa') != userAuthInfo()->id) {
            return to_route('2fa.verify');
        }

        return $next($request);
    }
}
