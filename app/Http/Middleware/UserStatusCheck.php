<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;
use Illuminate\Http\Request;

class UserStatusCheck
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->user() && userAuthInfo()->status == 0) {
            Auth::logout();
            toastr()->error(lang('Your account has been blocked', 'alerts'));

            return to_route('login');
        }

        return $next($request);
    }
}
