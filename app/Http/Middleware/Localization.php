<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\App;
use Closure;
use Illuminate\Http\Request;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     */
    public function handle(Request $request, Closure $next): \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
    {
        if (! settings('website_language_type') && $request->session()->has('locale')) {
            App::setLocale(session('locale'));
            App::setLocale(config('app.locale'));
        }

        return $next($request);
    }
}
