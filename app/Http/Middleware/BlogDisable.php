<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class BlogDisable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     */
    public function handle(Request $request, Closure $next): \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
    {
        abort_if(! settings('website_blog_status'), 404);

        return $next($request);
    }
}
