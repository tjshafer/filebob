<?php

use App\Http\Controllers\Backend;
use App\Http\Controllers\Frontend;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
 */

Route::middleware('notInstalled')->prefix('admin')->namespace('Backend')->group(function () {
    Route::name('admin.')->namespace('Auth')->group(function () {
        Route::get('/', [Backend\Auth\LoginController::class, 'redirectToLogin'])->name('index');
        Route::get('login', [Backend\Auth\LoginController::class, 'showLoginForm'])->name('login');
        Route::post('login', [Backend\Auth\LoginController::class, 'login'])->name('login.store');
        Route::get('password/reset', [Backend\Auth\ForgotPasswordController::class, 'showLinkRequestForm'])->name('password.reset');
        Route::post('password/reset', [Backend\Auth\ForgotPasswordController::class, 'sendResetLinkEmail']);
        Route::get('password/reset/{token}', [Backend\Auth\ResetPasswordController::class, 'showResetForm'])->name('password.reset.link');
        Route::post('password/reset/change', [Backend\Auth\ResetPasswordController::class, 'reset'])->name('password.reset.change');
        Route::post('logout', [Backend\Auth\LoginController::class, 'logout'])->name('logout');
    });
    Route::middleware('admin')->group(function () {
        Route::name('admin.')->middleware('demo')->group(function () {
            Route::prefix('dashboard')->group(function () {
                Route::get('/', [Backend\DashboardController::class, 'index'])->name('dashboard');
                Route::get('charts/users', [Backend\DashboardController::class, 'usersChartData'])->middleware('ajax.only');
                Route::get('charts/uploads', [Backend\DashboardController::class, 'uploadsChartData'])->middleware('ajax.only');
                Route::get('charts/earnings', [Backend\DashboardController::class, 'earningsChartData'])->middleware(['saas', 'ajax.only']);
                Route::get('charts/logs', [Backend\DashboardController::class, 'logsChartData'])->middleware('ajax.only');
            });
            Route::name('notifications.')->prefix('notifications')->group(function () {
                Route::get('/', [Backend\NotificationController::class, 'index'])->name('index');
                Route::get('view/{id}', [Backend\NotificationController::class, 'view'])->name('view');
                Route::get('readall', [Backend\NotificationController::class, 'readAll'])->name('readall');
                Route::delete('deleteallread', [Backend\NotificationController::class, 'deleteAllRead'])->name('deleteallread');
            });
            Route::name('users.')->prefix('users')->group(function () {
                Route::post('{id}/edit/change/avatar', [Backend\UserController::class, 'changeAvatar']);
                Route::delete('{id}/edit/delete/avatar', [Backend\UserController::class, 'deleteAvatar'])->name('deleteAvatar');
                Route::get('{id}/edit/logs', [Backend\UserController::class, 'logs'])->name('logs');
                Route::get('{id}/edit/logs/get/{log_id}', [Backend\UserController::class, 'getLogs'])->middleware('ajax.only');
                Route::post('{id}/edit/sentmail', [Backend\UserController::class, 'sendMail'])->name('sendmail');
                Route::get('logs/{ip}', [Backend\UserController::class, 'logsByIp'])->name('logsbyip');
            });
            Route::resource('users', 'UserController');
            Route::name('uploads.')->namespace('Uploads')->prefix('uploads')->group(function () {
                Route::get('secure/{id}', [Backend\Uploads\SecureUploadController::class, 'index'])->name('secure');
                Route::name('users.')->prefix('users')->group(function () {
                    Route::get('/', [Backend\Uploads\UserUploadsController::class, 'index'])->name('index');
                    Route::get('{shared_id}/view', [Backend\Uploads\UserUploadsController::class, 'view'])->name('view');
                    Route::get('{shared_id}/download', [Backend\Uploads\UserUploadsController::class, 'downloadFile'])->name('download');
                    Route::delete('{shared_id}/delete', [Backend\Uploads\UserUploadsController::class, 'destroy'])->name('destroy');
                    Route::post('delete-selected', [Backend\Uploads\UserUploadsController::class, 'destroySelected'])->name('destroy.selected');
                });
                Route::name('guests.')->prefix('guests')->group(function () {
                    Route::get('/', [Backend\Uploads\GuestUploadsController::class, 'index'])->name('index');
                    Route::get('{shared_id}/view', [Backend\Uploads\GuestUploadsController::class, 'view'])->name('view');
                    Route::get('{shared_id}/download', [Backend\Uploads\GuestUploadsController::class, 'downloadFile'])->name('download');
                    Route::delete('{shared_id}', [Backend\Uploads\GuestUploadsController::class, 'destroy'])->name('destroy');
                    Route::post('delete-selected', [Backend\Uploads\GuestUploadsController::class, 'destroySelected'])->name('destroy.selected');
                });
            });
            Route::name('reports.')->prefix('reports')->group(function () {
                Route::get('/', [Backend\FileReportController::class, 'index'])->name('index');
                Route::get('{id}/view', [Backend\FileReportController::class, 'view'])->name('view');
                Route::post('{id}/view', [Backend\FileReportController::class, 'markAsReviewed'])->name('markAsReviewed');
                Route::delete('{id}/delete', [Backend\FileReportController::class, 'destroy'])->name('destroy');
            });
            Route::middleware('saas')->group(function () {
                Route::resource('subscriptions', 'SubscriptionController');
                Route::resource('transactions', 'TransactionController');
                Route::resource('plans', 'PlanController');
                Route::resource('coupons', 'CouponController');
            });
            Route::get('advertisements', [Backend\AdvertisementController::class, 'index'])->name('advertisements.index');
            Route::get('advertisements/{id}/edit', [Backend\AdvertisementController::class, 'edit'])->name('advertisements.edit');
            Route::post('advertisements/{id}', [Backend\AdvertisementController::class, 'update'])->name('advertisements.update');
        });
        Route::prefix('navigation')->namespace('Navigation')->name('admin.')->middleware('demo')->group(function () {
            Route::post('navbarMenu/sort', [Backend\Navigation\NavbarMenuController::class, 'sort'])->name('navbarMenu.sort');
            Route::resource('navbarMenu', 'NavbarMenuController');
            Route::post('footerMenu/sort', [Backend\Navigation\FooterMenuController::class, 'sort'])->name('footerMenu.sort');
            Route::resource('footerMenu', 'FooterMenuController');
        });
        Route::prefix('blog')->namespace('Blog')->middleware('demo', 'disable.blog')->group(function () {
            Route::get('categories/slug', [Backend\Blog\CategoryController::class, 'slug'])->name('categories.slug');
            Route::resource('categories', 'CategoryController');
            Route::get('articles/slug', [Backend\Blog\ArticleController::class, 'slug'])->name('articles.slug');
            Route::get('articles/categories/{lang}', [Backend\Blog\ArticleController::class, 'getCategories'])->middleware('ajax.only');
            Route::resource('articles', 'ArticleController');
            Route::get('comments', [Backend\Blog\CommentController::class, 'index'])->name('comments.index');
            Route::get('comments/{id}/view', [Backend\Blog\CommentController::class, 'viewComment'])->middleware('ajax.only');
            Route::post('comments/{id}/update', [Backend\Blog\CommentController::class, 'updateComment'])->name('comments.update');
            Route::delete('comments/{id}', [Backend\Blog\CommentController::class, 'destroy'])->name('comments.destroy');
        });
        Route::prefix('settings')->namespace('Settings')->middleware('demo')->group(function () {
            Route::name('admin.settings.')->group(function () {
                Route::view('/', 'backend.settings.index')->name('index');
                Route::get('general', [Backend\Settings\GeneralController::class, 'index'])->name('general');
                Route::post('general/update', [Backend\Settings\GeneralController::class, 'update'])->name('general.update');
                Route::name('upload.')->prefix('upload')->middleware('noSaas')->group(function () {
                    Route::get('/', [Backend\Settings\UploadController::class, 'index'])->name('index');
                    Route::get('{id}', [Backend\Settings\UploadController::class, 'edit'])->name('edit');
                    Route::post('{id}', [Backend\Settings\UploadController::class, 'update'])->name('update');
                });
                Route::name('storage.')->prefix('storage')->group(function () {
                    Route::get('/', [Backend\Settings\StorageController::class, 'index'])->name('index');
                    Route::get('edit/{id}', [Backend\Settings\StorageController::class, 'edit'])->name('edit');
                    Route::post('edit/{id}', [Backend\Settings\StorageController::class, 'update'])->name('update');
                    Route::post('connect/{provider}', [Backend\Settings\StorageController::class, 'storageTest'])->name('test');
                    Route::post('default/{id}', [Backend\Settings\StorageController::class, 'setDefault'])->name('default');
                });
                Route::get('smtp', [Backend\Settings\SmtpController::class, 'index'])->name('smtp');
                Route::post('smtp/update', [Backend\Settings\SmtpController::class, 'update'])->name('smtp.update');
                Route::post('smtp/test', [Backend\Settings\SmtpController::class, 'test'])->name('smtp.test');
                Route::resource('extensions', 'ExtensionController')->only('index', 'edit', 'update');
                Route::resource('gateways', 'GatewayController')->only('index', 'edit', 'update')->middleware('saas');
                Route::name('mailtemplates.')->prefix('mailtemplates')->group(function () {
                    Route::get('/', [Backend\Settings\MailTemplateController::class, 'redirect'])->name('index');
                    Route::post('settings/update', [Backend\Settings\MailTemplateController::class, 'updateSettings'])->name('settings.update');
                    Route::get('{lang}', [Backend\Settings\MailTemplateController::class, 'index'])->name('show');
                    Route::get('{lang}/{group}', [Backend\Settings\MailTemplateController::class, 'index'])->name('show.group');
                    Route::post('{lang}/{group}', [Backend\Settings\MailTemplateController::class, 'update'])->name('update');
                });
                Route::resource('taxes', 'TaxController')->middleware('saas');
            });
            Route::get('pages/slug', [Backend\Settings\PageController::class, 'slug'])->name('pages.slug');
            Route::resource('pages', 'PageController');
            Route::resource('admins', 'AdminController');
            Route::prefix('languages')->group(function () {
                Route::post('settings/update', [Backend\Settings\LanguageController::class, 'updateSettings'])->name('language.settings.update');
                Route::post('{id}/default', [Backend\Settings\LanguageController::class, 'setDefault'])->name('language.default');
                Route::post('{id}/update', [Backend\Settings\LanguageController::class, 'translateUpdate'])->name('translates.update');
                Route::get('translate/{code}', [Backend\Settings\LanguageController::class, 'translate'])->name('language.translate');
                Route::get('translate/{code}/{group}', [Backend\Settings\LanguageController::class, 'translate'])->name('language.translate.group');
            });
            Route::resource('languages', 'LanguageController');
            Route::resource('seo', 'SeoController');
        });
        Route::name('admin.additional.')->prefix('additional')->namespace('Additional')->middleware('demo')->group(function () {
            Route::get('cache', [Backend\Additional\CacheController::class, 'index'])->name('cache');
            Route::get('custom-css', [Backend\Additional\CustomCssController::class, 'index'])->name('css');
            Route::post('custom-css/update', [Backend\Additional\CustomCssController::class, 'update'])->name('css.update');
            Route::get('popup-notice', [Backend\Additional\PopupNoticeController::class, 'index'])->name('notice');
            Route::post('popup-notice/update', [Backend\Additional\PopupNoticeController::class, 'update'])->name('notice.update');
        });
        Route::name('admin.')->prefix('others')->namespace('Others')->middleware('demo')->group(function () {
            Route::resource('slideshow', 'SlideShowController');
            Route::resource('features', 'FeatureController');
            Route::resource('faq', 'FaqController');
            Route::get('download-page', [Backend\Others\DownloadPageController::class, 'index']);
            Route::post('download-page', [Backend\Others\DownloadPageController::class, 'update'])->name('download.page');
        });
        Route::name('admin.')->prefix('account')->namespace('Account')->middleware('demo')->group(function () {
            Route::get('details', [Backend\Account\SettingsController::class, 'detailsForm'])->name('account.details');
            Route::get('security', [Backend\Account\SettingsController::class, 'securityForm'])->name('account.security');
            Route::post('details/update', [Backend\Account\SettingsController::class, 'detailsUpdate'])->name('account.details.update');
            Route::post('security/update', [Backend\Account\SettingsController::class, 'securityUpdate'])->name('account.security.update');
        });
    });
});

/*
|--------------------------------------------------------------------------
| Frontend Routs With Laravel Localization
|--------------------------------------------------------------------------
 */
Route::get('secure/file/{id}', [Frontend\File\SecureController::class, 'index'])->name('secure.file');
