<?php

return [
    '$key' => env($key),
    'default_language' => env('DEFAULT_LANGUAGE'),
    'demo_mode' => env('DEMO_MODE'),
    'facebook_client_id' => env('FACEBOOK_CLIENT_ID'),
    'facebook_client_secret' => env('FACEBOOK_CLIENT_SECRET'),
    'licence_type' => env('LICENCE_TYPE'),
    'vironeer_systemstatus' => env('VIRONEER_SYSTEMSTATUS'),
];
